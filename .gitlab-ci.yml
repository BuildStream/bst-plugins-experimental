include:
  - template: Code-Quality.gitlab-ci.yml
  - template: SAST.gitlab-ci.yml

image: registry.gitlab.com/buildstream/buildstream-docker-images/testsuite-fedora:40-${DOCKER_IMAGE_VERSION}

cache:
  key: "$CI_JOB_NAME-"
  paths:
    - cache/

default:
  interruptible: true

stages:
  - test
  - upload

variables:
  # SAST related variables
  SAST_DISABLE_DIND: "true"
  # only run a subset of default analyzers from:
  # https://docs.gitlab.com/ee/user/application_security/sast/analyzers.html
  SAST_DEFAULT_ANALYZERS: "bandit, secrets"
  SAST_EXCLUDED_PATHS: "tests/*"
  SAST_GOSEC_LEVEL: 2
  CI_PROJECT_REPOSITORY_LANGUAGES: "python"

  FF_TIMESTAMPS: 1

  # Our own variables
  # Version of the docker images we should use for all the images.
  # This is taken from buildstream/buildstream-docker-images and is updated
  # periodically.
  DOCKER_IMAGE_VERSION: master-1392822106
  PYTEST_ADDOPTS: "--color=yes"
  INTEGRATION_CACHE: "${CI_PROJECT_DIR}/cache/integration-cache"
  PYTEST_ARGS: "--color=yes --integration"
  TEST_COMMAND: "tox -- ${PYTEST_ARGS}"

#####################################################
#                    Test stage                     #
#####################################################

# Run premerge commits
#
.tox-template:
  stage: test

  before_script:
  # Diagnostics
  - mount
  - df -h
  - python3 -mpip install tox-wheel
  - tox --version

  script:
  - mkdir -p "${INTEGRATION_CACHE}"
  - "${TEST_COMMAND}"

test-fixed-debian-11:
  extends:
  - .tox-template
  image: registry.gitlab.com/buildstream/buildstream-docker-images/testsuite-debian:11-${DOCKER_IMAGE_VERSION}
  variables:
    TOXENV: "py39-bst-fixed"

test-fixed-fedora-40:
  extends:
  - .tox-template
  image: registry.gitlab.com/buildstream/buildstream-docker-images/testsuite-fedora:40-${DOCKER_IMAGE_VERSION}
  variables:
    TOXENV: "py312-bst-fixed"

# NOTE: Tests against master are only run against one distribution in
# order to conserve CI resources.
test-master-fedora-40:
  image: registry.gitlab.com/buildstream/buildstream-docker-images/testsuite-fedora:40-${DOCKER_IMAGE_VERSION}
  extends:
  - .tox-template
  variables:
    TOXENV: "py312-bst-master"
  allow_failure: true

docs:
  extends:
  - .tox-template
  script:
  - tox -e docs
  artifacts:
    paths:
    - doc/build/html

lint:
  extends:
  - .tox-template
  script:
  - tox -e lint

format-check:
  extends:
  - .tox-template
  script:
  - tox -e format-check

#####################################################
#               Upload stage                        #
#####################################################

# Release documentation on tags.
#
pages:
  stage: upload
  dependencies:
  - docs
  script:
  - mv doc/build/html public
  artifacts:
    paths:
    - public/
  only:
  - tags

# Automatically upload to PyPI when a release is tagged.
#
pypi-release:
  stage: upload
  before_script:
  - |
    python3 -mvenv venv
    ./venv/bin/pip install twine build
  script:
  - |
    ./venv/bin/python3 -m build .
  - |
    ./venv/bin/twine upload --skip-existing dist/*
  only:
  - /[0-9]+\.[0-9]+\.[0-9]+/
  except:
  - branches

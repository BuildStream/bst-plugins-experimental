.. toctree::
   :maxdepth: 2

buildstream-plugins-community Documentation
===========================================

This is a collection of plugins to use with Buildstream

To use one of these plugins in your project you need to have installed the
buildstream-plugins-community package and enabled it in your `project configuration file
<https://buildstream.gitlab.io/buildstream/projectconf.html#plugin-origins-and-versions>`_.

.. toctree::
   :maxdepth: 1
   :caption: Contained Elements

   elements/bazel_build
   elements/bazelize
   elements/check_forbidden
   elements/dpkg_build
   elements/dpkg_deploy
   elements/x86image
   elements/flatpak_image
   elements/flatpak_repo
   elements/collect_integration
   elements/collect_manifest
   elements/fastboot_bootimg
   elements/fastboot_ext4
   elements/snap_image
   elements/tar_element
   elements/makemaker
   elements/modulebuild
   elements/qmake
   elements/oci
   elements/pyproject
   elements/ostree

.. toctree::
   :maxdepth: 1
   :caption: Contained Sources

   sources/bazel_source
   sources/cargo2
   sources/cpan
   sources/deb
   sources/ostree
   sources/quilt
   sources/git_tag
   sources/git_module
   sources/git_repo
   sources/go_module
   sources/pypi
   sources/zip
   sources/patch_queue
   sources/gnu
   sources/gen_cargo_lock
